// Exchange Rate
// Author: Alex Judy
// Date: 1/14/2014
// Section: 1

#include <stdio.h>

int main()
 {
 int userInput;
 /* Currencies 
 [US Dollar]                  [British Pound]              [Canadian Dollar]            [Euro]                       [Australian Dollar]          [New Zealand Dollar]                    */
 const float USD=1.0;         const float GBPtoUSD=1.6479; const float CADtoUSD=0.9115; const float EURtoUSD=1.3563; const float AUDtoUSD=0.8864; const float NZDtoUSD=0.8317;
 const float USDtoGBP=0.6083; const float GBP=1.0;         const float CADtoGBP=0.5531; const float EURtoGBP=0.8231; const float AUDtoGBP=0.5379; const float NZDtoGBP=0.5049;
 const float USDtoCAD=1.0952; const float GBPtoCAD=1.8079; const float CAD=1.0;         const float EURtoCAD=1.4881; const float AUDtoCAD=0.9726; const float NZDtoCAD=0.9129;
 const float USDtoEUR=0.7311; const float GBPtoEUR=1.2150; const float CADtoEUR=0.6720; const float EUR=1.0;         const float AUDtoEUR=0.6537; const float NZDtoEUR=0.6135;
 const float USDtoAUD=1.1159; const float GBPtoAUD=1.8592; const float CADtoAUD=1.0284; const float EURtoAUD=1.5301; const float AUD=1.0;         const float NZDtoAUD=0.9389;
 const float USDtoNZD=1.2024; const float GBPtoNZD=1.9806; const float CADtoNZD=1.0954; const float EURtoNZD=1.6300; const float AUDtoNZD=1.0651; const float NZD=1.0;
 
 printf("Exchange Rates \n");
 printf("Enter your value: \n");
 scanf("%d", &userInput) ;   

	  printf("|-----------------------------------------------------------|\n");
      printf("|     | USD    | GBP    | CAD    | EUR    | AUD    | NZD    |\n");
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| USD |%d        %.2f     %.2f     %.2f     %.2f     %.2f   \n"   ,userInput ,userInput*GBPtoUSD ,userInput*CADtoUSD ,userInput*EURtoUSD ,userInput*AUDtoUSD ,userInput*NZDtoUSD);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| GBP |%.2f     %d        %.2f     %.2f     %.2f     %.2f    \n"   ,userInput*USDtoGBP ,userInput ,userInput*CADtoGBP ,userInput*EURtoGBP ,userInput*AUDtoGBP ,userInput*NZDtoGBP);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| CAD |%.2f     %.2f     %d        %.2f     %.2f     %.2f    \n"   ,userInput*USDtoCAD ,userInput*GBPtoCAD ,userInput ,userInput*EURtoCAD ,userInput*AUDtoCAD ,userInput*NZDtoCAD);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| EUR |%.2f     %.2f     %.2f     %d        %.2f     %.2f    \n"   ,userInput*USDtoEUR ,userInput*GBPtoEUR ,userInput*CADtoEUR ,userInput ,userInput*AUDtoEUR ,userInput*NZDtoEUR);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| AUD |%.2f     %.2f     %.2f     %.2f     %d        %.2f    \n"   ,userInput*USDtoAUD ,userInput*GBPtoAUD ,userInput*CADtoAUD ,userInput*EURtoAUD ,userInput ,userInput*NZDtoAUD);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      printf("| NZD |%.2f     %.2f     %.2f     %.2f     %.2f     %d      \n"   ,userInput*USDtoNZD ,userInput*GBPtoNZD ,userInput*CADtoNZD ,userInput*EURtoNZD ,userInput*AUDtoNZD ,userInput);
      printf("|-----|--------|--------|--------|--------|--------|--------|\n");
      
 system("PAUSE");
 return 0;
} //End Program
