/*
Alex Judy
1792945
1/25/16
CECS 420
Project 1
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    char* word;
    int count;
    struct node* next;
} node;

//Notes to self: Make sure to use argv[] for files as user command
//Strip whitespace (fun: sanitize) and fgets to grab line by line
//Should track position of word and what should NOT be read. \t \n ' ' etc...

int sanitize_file(char* word)
{
	int i = 0;
	int word_good = 0;
	for(i = 0; i < strlen(word); i++)
	{
		if(word[i] != ' ' && word[i] != '\t' && word[i] != '\n')
		{
			word_good = 1;
			break;
		}
	}
	if(word_good == 1)
	{
		char *check1 = word, *check2 = word;
		do
		{
			while ((*check2) == ' ' || (*check2) == '\t' || (*check2) == '\n')
			{
				check2++;
			}
		}
		while ((*check1++ = *check2++));
		return 1;
	}
	else
	{
		return -1;
	}
}

void insert_node(node** head, node* next)
{
	if((*head) == NULL || strcmp((*head)->word, next->word) > 0)
	{   
		next->next = (*head);
		(*head) = next;
	}
	else
	{           
		node* curr = *head;
		while (curr->next != NULL && strcmp(curr->next->word, next->word) < 0)
		{
			curr = curr->next;
		}
	next->next = curr->next;
	curr->next = next;
	}
}

int add_node(node** head, char* word, int count)
{
	node* next = malloc(sizeof(node));
	if(next == NULL)
	{
		free(next);
		return -1;
	}
	next->word = strndup(word, strlen(word) + 1);
	next->count = count;
	next->next = NULL;
	insert_node(head, next);

    return 1;
}

void print_list(node** head, char* file)
{
	FILE* output = fopen(file, "w");
	node* curr = *head;
	while(curr != NULL)
	{
		fprintf(output, "%s,%i\n",curr->word, curr->count);
		curr = curr->next;
	}
	fclose(output);
return;
}

void destroy_list(node** head)
{  
	node* current = *head;
	node* next;
	while (current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
	*head = NULL;
}

int dup_count(int words_grabbed, node** head, char* word)
{
	if(words_grabbed == 0)
	{
		return 1;
	}
	node* curr = *head; 
	while(curr != NULL)
	{
		if(strcmp(curr->word, word) == 0)
		{
			(curr->count)++;
			return -1;
		}
		curr = curr->next;
	}
	return 1;
}

void merge_list(node** list1, node** list2, node** outlist)
{
	node* curr1 = *list1;
	node* curr2 = *list2;
	while(curr1 != NULL && curr2 != NULL)
	{
		if(strcmp(curr1->word, curr2->word) == 0)
		{
			add_node(outlist, curr1->word, curr1->count + curr2->count);
			curr1 = curr1->next;
			curr2 = curr2->next;
		}
		else if(strcmp(curr1->word, curr2->word) < 0)
		{
			curr1 = curr1->next;
		}
		else
		{
			curr2 = curr2->next;
		}
	}
}

void get_words(node** head, char* file)
{
	char* line = malloc(256);
	char* word;
	int i = 0;
	int words_grabbed = 0;
	FILE *input = fopen(file, "r");
	if(input == NULL)
	{
		printf("Unable to open file %s\n", file);		
		return; 
	}
	if(line == NULL)
	{
		free(line);
		return;
	}
	while(fgets(line, 256, input) != NULL)
	{
		word = strtok(line, " ");
		while(word != NULL)
		{
			if(sanitize_file(word) == 1)
			{  
				if(dup_count(words_grabbed, head, word) == 1)
				{
					i = add_node(head, word, 1);
					words_grabbed++;
					if(i != 1)
					{
						free(line);
						return;
					}
				}
			}
			else
			{
				word = NULL;
			}
			word = strtok(NULL, " ");
		}
	}
	free(line);
	fclose(input);
}

int main(int argc, char** argv)
{
    if(argc != 4)
    {
	printf("Sorry, you need at least two input files and one output file.\n");
        return -1;
    }
	node* list1 = NULL;
	node* list2 = NULL;
	node* outlist = NULL;
	get_words(&list1, argv[1]);
	get_words(&list2, argv[2]);
	merge_list(&list1, &list2, &outlist);
	print_list(&outlist, argv[3]);
	destroy_list(&list1);
	destroy_list(&list2);
	destroy_list(&outlist);
     return 0;
}
