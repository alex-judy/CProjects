// Story Generator
// Author: Alex Judy
// Date: 2/10/2014
// Section: 1

#include<stdio.h>
#include<stdlib.h>
#include<time.h> 


char car[14] = {'\0'};
char* ca = car;

char name[30] = {'\0'};
char* na = name;

int age[3] = {'\0'};
int* ag = age;


int main() 
{

	int userInput=1;
	char randomCol, randomAct, randomRes;
	srand(time(NULL));
	randomCol = rand()%10;
	randomAct = rand()%6;
	randomRes = rand()%6;
	
	printf("Story Generator\n");
	printf("\n(1) To create a new story.\n");
	printf("\n(2) To exit the program.\n");
	printf("\nPlease choose an option: ");
		scanf("%d", &userInput);
	
	switch (userInput)
	{
		case 1:
			
	    {
		char *Col[10]={"red", "blue", "yellow", "green", "black", "white", "gray", "brown", "orange", "pink"};
		char *Act[6]={"baseball", "basketball", "football", "golf", "video games", "tennis"};
		char *Res[6]={"Cracker Barrel", "Denny's", "Hardee's", "Wendy's", "Subway", "Olive Garden"};
		
			printf("\nWhat is your first name: ");
				scanf("%s", name);
			printf("\nHow old are you: ");
				scanf("%s", age);
			printf("\nWhat is your favorite car: ");
				scanf("%s", car);
	    
			
			
				printf("\n%s was driving their %s through the neighborhood.\n",na ,ca);
				printf("On their way out of town they saw a %s birthday card.\n", Col[randomCol]);
				printf("The card read that they were %s years old.\nThey were supposed to meet their friends at %s.\n", ag, Res[randomRes]);
				printf("After the birthday party they played %s with their friends.\n", Act[randomAct]);
				return main();
		break;	
		}

		case 2:
			
		{
		printf("\nGood Bye!");
		return 0;
		break;
	    }
	    
		default:
		printf("You have entered an invalid input. Please select select a different option.\n");
		return main();
		break;	
    }
    
system("PAUSE");
return 0;
}
