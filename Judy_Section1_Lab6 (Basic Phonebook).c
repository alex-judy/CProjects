// Basic Phonebook
// Author: Alex Judy
// Date: 2/16/2014
// Section: 1

#include<stdio.h>
#include<stdlib.h>

    void addUser();
    void removeUser();
    void displayUser();
    
    typedef struct {
            char *firstName;
            char *lastName;
            char *phoneNumber;
            } phoneBook;
            
    phoneBook *pb;
    
    int count = 0;
    
    main() {
           int userValue = 0;
           while (userValue != 4) {
                 printf("Phone Book Application\n");
                 printf("(1) Add friend\n");
                 printf("(2) Remove friend\n");
                 printf("(3) Show phone book\n");
                 printf("(4) Exit\n\n");
                 
                 printf("What do you want to do? ");
                 scanf("%d", &userValue);
                 printf("\n");
                 
           switch (userValue) {
                  case 1:
                       addUser();
                       break;
                  case 2:
                       removeUser();
                       break;
                  case 3:
                       displayUser();
                       break;
                  case 4:
                       printf("Good Bye!\n");
                        break;
                 }
           }
           
           pb = NULL;
           free(pb);     
           system("pause");
       }

    void addUser() {
         if (count == 0) {
            pb = (phoneBook *) malloc(sizeof(phoneBook) + (count*40) + 40);
         } else {
            pb = (phoneBook *) realloc(pb, sizeof(phoneBook) + (count*40) + 40);
             }
             
         if (pb == NULL) {
                printf("You cannot add more friends: OUT OF MEMORY\n");
         } else {
             pb[count].firstName = (char *) malloc(sizeof(char) * 15);
             pb[count].lastName = (char *) malloc(sizeof(char) * 15);
             pb[count].phoneNumber = (char *) malloc(sizeof(char) * 10);
             printf("First name: ");
             scanf("%s", pb[count].firstName);
             printf("Last name: ");
             scanf("%s", pb[count].lastName);
             printf("Phone Number: ");
             scanf("%s", pb[count].phoneNumber);
             
             count++;
             printf("\nRecord added to your Phone Book!\n\n");
         }
    }
    
    void removeUser() {
         char *fName = (char *) malloc(sizeof(char) * 15);;
         char *lName = (char *) malloc(sizeof(char) * 15);;
         int i;
         int isRemoved = 0;
         
         printf("First name: ");
         scanf("%s", fName);
         printf("Last name: ");
         scanf("%s", lName);
         
         for (i = 0; i < count; i++) {
             if ((strcmp(pb[i].firstName, fName) == 0) && (strcmp(pb[i].lastName, lName) == 0)) {
                pb[i].firstName = NULL;
                pb[i].lastName = NULL;
                pb[i].phoneNumber = NULL;
                isRemoved = 1;
                break;                     
             } 
         }
         
         if (isRemoved == 1) {
            printf("\nRecord deleted from your Phone Book.\n\n");
         } else {
            printf("\nRecord not found.\n\n");
         }
    }
    
    void displayUser() {
         int i;
         int printed = 0;
         
         printf("Phone Book: \n");
         for (i = 0; i < count; i++) {
             if ((pb[i].firstName != NULL) && (pb[i].lastName != NULL) && (pb[i].phoneNumber != NULL)) {
                printf("\n(%d)\t%s\t%s\t%s\n", printed + 1, pb[i].firstName, pb[i].lastName, pb[i].phoneNumber);
                printed++;
             } 
         }
         printf("\n");
         if (!printed) {
            printf("No entries found!\n\n");
            }
    }
